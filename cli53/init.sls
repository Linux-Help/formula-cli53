{%- from slspath + '/map.jinja' import cli53 with context -%}

cli53-bin-dir:
  file.directory:
    - name: /usr/local/bin
    - makedirs: True

# Install cli53
cli53-download:
  file.managed:
    - name: /usr/local/bin/cli53-linux-{{ cli53.arch }}
    - source: https://github.com/barnybug/cli53/releases/download/{{ cli53.version }}/cli53-linux-{{ cli53.arch }}
    - source_hash: https://github.com/barnybug/cli53/releases/download/{{ cli53.version }}/cli53_{{ cli53.version }}_checksums.txt
    - user: root
    - group: root
    - mode: 755
    - require:
      - file: cli53-bin-dir

# Link cli53
cli53-link:
  file.symlink:
    - target: cli53-linux-{{ cli53.arch }}
    - name: /usr/local/bin/cli53
    - watch:
      - file: cli53-download

