describe file('/usr/local/bin/cli53-linux-amd64') do
  it { should exist }
end

describe file('/usr/local/bin/cli53') do
  it { should be_symlink }
  its('link_path') { should eq 'cli53-linux-amd64' }
end
